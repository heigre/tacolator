# The Tacolator

<img src="./images/tacolator.png">

Tacolator is a python web-based calculator that helps you determine how much taco ingredients you need based on the number of people you plan to serve. This project uses Docker Compose to run both the Tacolator web server and the API interface called taco-server.

This is a work-in-progress project.

</br>

## Taco-core
<img src="./images/tacocore.png" width="200">

Taco-core is the core of the Tacolator web server. It contains the code responsible for processing user inputs, calculating the amount of taco ingredients needed based on the number of people being served, and returning the results to the user. The Tacolator web server is built on top of taco-core, allowing users to access the taco ingredient calculation functionality through a simple and intuitive web interface.

</br>

## Taco-server
<img src="./images/tacoserver.png" width="200">

taco-server is a server hosting an API interface that provides the same functionality as taco-core for external systems to access Tacolator's calculations. It is designed to be used by developers who want to integrate Tacolator's functionality into their own applications or systems. By providing a simple and straightforward API, taco-server makes it easy for external systems to leverage the power of Tacolator's taco ingredient calculation engine. Together, taco-core and taco-server form the backbone of the Tacolator project, allowing users to calculate the amount of ingredients needed for their next taco night.


</br>

## Installation and Setup

To run Tacolator, you'll need to have Docker and Docker Compose installed on your machine. Once you've installed Docker and Docker Compose, follow these steps:

    Clone this repository to your local machine.
    Navigate to the project root directory.
    Build the Docker image by running docker-compose build
    Run the Docker container by running docker-compose up

This will start the Tacolator web server, which you can access by navigating to http://localhost:8080 in your web browser.

To access the taco-server API interface, you can navigate to http://localhost:5000/api/tacolator.


</br>

## Usage

To use Tacolator, enter the number of people you plan to serve in the input field on the home page and click the "Calculate" button. Tacolator will then provide you with the amount of ingredients you'll need to make tacos for that number of people.

</br>

## Project Structure

The main file for Tacolator is located at ./taco_core/src/main.py. This file contains the code for the Tacolator web server.

The taco-server API interface is located at ./taco-server/src/main.py. This file contains the code for the API interface that provides the taco ingredients calculation functionality.




## Endpoints
### /taco
Method: POST

Calculate the amount of taco ingredients needed based on the number of people being served.

Request

* people - integer - The number of people being served.

```
{
  "people": 10
}
```

Response

* success - boolean - Whether the request was successful.
* data - object - An object containing the calculated amounts of ingredients.

```
{
  "success": true,
  "data": {
    "tortillas": 20,
    "ground_beef": 2,
    "lettuce": 1,
    "cheese": 0.5,
    "salsa": 0.25
  }
}
```

### /docs
Method: GET

OpenAPI documentation for the API.

### /redoc

Method: GET

ReDoc documentation for the API.


## API Example Usage

```
import requests

data = {"people": 10}
response = requests.post("http://localhost:8000/taco", json=data)
result = response.json()

if response.status_code == 200 and result["success"]:
    print("Taco ingredients:")
    for ingredient, amount in result["data"].items():
        print(f"{ingredient.capitalize()}: {amount}")
else:
    print("Failed to calculate taco ingredients.")
```

Response Codes

    200 OK - The request was successful.
    400 Bad Request - The request was invalid or missing required data. The response will include an error message.
    500 Internal Server Error - The server encountered an error while processing the request. The response will include an error message.



</br>

## Contributing

If you'd like to contribute to Tacolator, please fork this repository and submit a pull request with your changes. We welcome any contributions that improve the functionality or usability of the project.

</br>

## License

Tacolator is licensed under the GNU GPL-3.0 license. See LICENSE for more information.